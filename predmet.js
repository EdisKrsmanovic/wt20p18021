const Sequelize = require("sequelize");
 
module.exports = function (sequelize, DataTypes) {
    const Predmet = sequelize.define('Predmet', {
        naziv: {
            type: Sequelize.STRING,
            unique: true,
            allowNull: false
        },
   });
   return Predmet;
}
