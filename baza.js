const Sequelize = require("sequelize");
const sequelize = new Sequelize("wt2018021", "root", "root", {
   host: "127.0.0.1",
   dialect: "mysql"
});
const db={};

db.Sequelize = Sequelize;  
db.sequelize = sequelize;

//import modela
require('./predmet.js')(sequelize, Sequelize.DataTypes);
db.predmet = sequelize.models.Predmet;

require('./grupa.js')(sequelize, Sequelize.DataTypes);
db.grupa = sequelize.models.Grupa;

require('./aktivnost.js')(sequelize, Sequelize.DataTypes);
db.aktivnost = sequelize.models.Aktivnost;

require('./dan.js')(sequelize, Sequelize.DataTypes);
db.dan = sequelize.models.Dan;

require('./tip.js')(sequelize, Sequelize.DataTypes);
db.tip = sequelize.models.Tip;

require('./student.js')(sequelize, Sequelize.DataTypes);
db.student = sequelize.models.Student;

// require('./studentGrupa.js')(sequelize, Sequelize.DataTypes);
// db.studentGrupa = sequelize.models.StudentGrupa;


//relacije
// db.imenik.belongsToMany(db.imenik, {
//    through: sequelize.models.Adresar,
//    as: 'Kontakt',
//    foreignKey: 'idPoznanik'
// });
// db.imenik.belongsToMany(db.imenik, {
//    through: sequelize.models.Adresar,
//    as: 'Poznanik',
//    foreignKey: 'idKontakta'
// });
db.predmet.hasMany(db.grupa, { foreignKey: { allowNull: false }});
db.predmet.hasMany(db.aktivnost, { foreignKey: { allowNull: false }});
db.grupa.hasMany(db.aktivnost);
db.dan.hasMany(db.aktivnost, { foreignKey: { allowNull: false }});
db.tip.hasMany(db.aktivnost, { foreignKey: { allowNull: false }});
db.student.belongsToMany(db.grupa, {
   through: 'StudentGrupa',
   as: 'Grupe',
   foreignKey: 'idStudenta'
});
db.grupa.belongsToMany(db.student, {
   through: 'StudentGrupa',
   as: 'Studenti',
   foreignKey: 'idGrupe'
});
module.exports=db;