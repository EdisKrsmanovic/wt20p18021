const assert = require('assert');
const fs = require('fs');
const request = require('sync-request');

describe('Testovi iz datoteke testniPodaci.txt', function() {
    let file = [];
    try {
        file = fs.readFileSync('test/testniPodaci.txt', 'utf-8');
    } catch(error) {
        console.log('Datoteka testniPodaci.txt ne postoji ili nije u test direktoriju');
        return;
    }

    const lines = file.split('\n');
    
    let brTesta = 1;

    for(let line of lines) {
        line = line.replace(/\\/g, '');
        line = line.replace(/”/g, '"');
        line = line.replace(/\[\[/g, '[');
        line = line.replace(/\]\]/g, ']');
        console.log(line);
        const vrijednosti = line.split(',');
        it(brTesta + '. test (' + vrijednosti[0] + ' ' + vrijednosti[1] + ')', function() {

            let j = 3;
            if(vrijednosti.length > 4) {
                for(let i = 3; i < vrijednosti.length; i++) {
                    if(vrijednosti[2].includes('{') && !vrijednosti[2].includes('}')) {
                        vrijednosti[2] += ',' + vrijednosti[i];
                        j = i+1;
                    } else {
                        if(j!=i) vrijednosti[j] += ',' + vrijednosti[i];
                    }
                }
            }
            body = vrijednosti[2];
            vrijednosti[3] = vrijednosti[j];
            let response = request(vrijednosti[0], 'http://localhost:3000' + vrijednosti[1], {
                json: JSON.parse(body),
            });

            let actualBody = response.getBody('utf8');

            assert.strictEqual(actualBody, vrijednosti[3]);
        });
        brTesta++;
    }
});