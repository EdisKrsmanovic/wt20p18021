const Sequelize = require("sequelize");
 
module.exports = function (sequelize, DataTypes) {
    const Tip = sequelize.define('Tip', {
        naziv: {
            type: Sequelize.STRING,
            unique: true,
            allowNull: false
        },
   });
   return Tip;
}
