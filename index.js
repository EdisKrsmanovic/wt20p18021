const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');
const app = express();
const pocetakRasporeda = 8;
const krajRasporeda = 20;
const db = require('./baza.js');

db.sequelize.sync({force:true}).then(async function() {
    await db.dan.bulkCreate([{naziv: "Ponedjeljak"}, {naziv: "Utorak"}, {naziv: "Srijeda"}, {naziv: "Četvrtak"}, {naziv: "Petak"}]);
    console.log('Zavrsio sa ucitavanjem baze');
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static('public'));

// #region Spirala 3
// #region Zadatak 2

function ucitajIzFajla(nazivDatoteke, nazivPredmeta) {
    let file = [];
    try {
        file = fs.readFileSync(nazivDatoteke, 'utf-8');
    } catch(error) {
        return 'Datoteka ' + nazivDatoteke + ' ne postoji';
    }
    const lines = file.split('\n');

    let data = [];

    for (let line of lines) {
        if(line != '') {
            if(nazivDatoteke == 'aktivnosti.txt') {
                const vrijednosti = line.split(',');
                if(vrijednosti.length == 5) {
                    if(nazivPredmeta == undefined || nazivPredmeta == vrijednosti[0]) {
                        data.push(
                            {
                                naziv: vrijednosti[0], 
                                tip: vrijednosti[1], 
                                pocetak: parseFloat(vrijednosti[2]), 
                                kraj: parseFloat(vrijednosti[3]), 
                                dan: vrijednosti[4]
                            }
                        );
                    }
                } else {
                    return 'Neispravan format datoteke';
                }
            } else if(nazivDatoteke == 'predmeti.txt') {
                data.push({naziv: line});
            }
        }
    }

    return data;
}

app.get('/v1/predmeti', function(req, res) {
    let predmeti = ucitajIzFajla('predmeti.txt');

    return res.json(predmeti);
});

app.get('/v1/aktivnosti', function(req, res) {
    let aktivnosti = ucitajIzFajla('aktivnosti.txt');

    return res.json(aktivnosti);
});

app.get('/v1/predmet/:naziv/aktivnost/', function(req, res) {
    let nazivPredmeta = req.params.naziv;

    let aktivnosti = ucitajIzFajla('aktivnosti.txt', nazivPredmeta); 

    return res.json(aktivnosti);
});

app.post('/v1/predmet', function(req, res) {
    let predmeti = ucitajIzFajla('predmeti.txt');

    let noviPredmet = req.body;

    for(let predmet of predmeti) {
        if(predmet.naziv.toLowerCase() == noviPredmet.naziv.toLowerCase()) {
            return res.json({message: 'Naziv predmeta postoji!'})
        }
    }

    let newLine = '\n';
    if(predmeti.length == 0) {
        newLine = '';
    }

    fs.appendFileSync('predmeti.txt', newLine + noviPredmet.naziv);

    return res.json({message: 'Uspješno dodan predmet!'});
});

app.post('/v1/aktivnost', function(req, res) {
    let aktivnosti = ucitajIzFajla('aktivnosti.txt');

    let novaAktivnost = req.body;

    // #region VALIDACIJA
    const dani = ['Ponedjeljak', 'Utorak', 'Srijeda', 'Četvrtak', 'Petak'];
    if(novaAktivnost.pocetak >= novaAktivnost.kraj 
    || novaAktivnost.pocetak%0.5 != 0 
    || novaAktivnost.kraj%0.5 != 0 
    || novaAktivnost.pocetak == null
    || novaAktivnost.kraj == null
    || novaAktivnost.pocetak < pocetakRasporeda
    || novaAktivnost.kraj > krajRasporeda
    || dani.indexOf(novaAktivnost.dan) == -1 
    || novaAktivnost.naziv == '' 
    || novaAktivnost.tip == ''
    || novaAktivnost.naziv == null
    || novaAktivnost.tip == null) {
        return res.json({message: 'Aktivnost nije validna!'});
    }

    for(let aktivnost of aktivnosti) {
        if(aktivnost.dan == novaAktivnost.dan) {
            if((aktivnost.pocetak == novaAktivnost.pocetak && aktivnost.kraj == novaAktivnost.kraj) 
            || (aktivnost.pocetak < novaAktivnost.pocetak && aktivnost.kraj > novaAktivnost.pocetak)    // |    p |    k 
            || (aktivnost.pocetak < novaAktivnost.kraj && aktivnost.kraj > novaAktivnost.kraj)          // p    | k    |  
            || (aktivnost.pocetak > novaAktivnost.pocetak && aktivnost.kraj < novaAktivnost.kraj)       // |    p k    |
            || (aktivnost.pocetak < novaAktivnost.pocetak && aktivnost.pocetak > novaAktivnost.kraj)) { // p    | |    k
                return res.json({message: 'Aktivnost nije validna!'});
            }
        }
    }
    // #endregion

    let newLine = '\n';
    if(aktivnosti.length == 0) {
        newLine = '';
    }

    fs.appendFileSync('aktivnosti.txt', newLine + novaAktivnost.naziv + ',' + novaAktivnost.tip + ',' + novaAktivnost.pocetak + ',' + novaAktivnost.kraj + ',' + novaAktivnost.dan);
    return res.json({message: 'Uspješno dodana aktivnost!'});
});

function obrisiAktivnosti(nazivAktivnosti, poruka, res) {
    fs.readFile('aktivnosti.txt', function(err, data) {
        if(err) {
            return res.json({message:'Greška - aktivnost nije obrisana!'});
        } else {
            let regex1 = new RegExp('\n(' + nazivAktivnosti + '.*)', 'g');
            let newData = data.toString().replace(regex1, '');
            let regex2 = new RegExp('(' + nazivAktivnosti + '.*)', 'g'); //Ako aktivnost postaje jedina aktivnost u fajlu, mora se izvrsiti regex bez \n;
            newData = newData.toString().replace(regex2, '');
            if(newData[0] == '\n') {
                newData = newData.substring(1,newData.length);
            }
            fs.writeFile('aktivnosti.txt', newData, function(err) {
                if(err) {
                    return res.json({message:'Greška - aktivnost nije obrisana!'});
                } else {
                    return res.json({message: poruka});
                }
            });
        }
    });
}

app.delete('/v1/aktivnost/:naziv', function(req, res) {
    let nazivAktivnosti = req.params.naziv;
    obrisiAktivnosti(nazivAktivnosti, 'Uspješno obrisana aktivnost!', res);
});

app.delete('/v1/predmet/:naziv', function(req, res) {
    let nazivPredmeta = req.params.naziv;
    fs.readFile('predmeti.txt', function(err, data) {
        if(err) {
            return res.json({message:'Greška - predmet nije obrisan!'});
        } else {
            let regex1 = new RegExp('\n(' + nazivPredmeta + '.*)', 'g');
            let newData = data.toString().replace(regex1, '');
            let regex2 = new RegExp('(' + nazivPredmeta + '.*)', 'g'); //Ako nazivPredmeta postaje jedini predmet u fajlu, mora se izvrsiti regex bez \n;
            newData = newData.toString().replace(regex2, '');
            fs.writeFile('predmeti.txt', newData, function(err) {
                if(err) {
                    return res.json({message:'Greška - predmet nije obrisan!'});
                } else {
                    obrisiAktivnosti(nazivPredmeta, 'Uspješno obrisan predmet!', res);
                }
            });
        }
    });
});

app.delete('/v1/all', function(req, res) {
    fs.truncate('aktivnosti.txt', 0, function() {
        fs.truncate('predmeti.txt', 0, function() {
            return res.json({message:'Uspješno obrisan sadržaj datoteka!'});
        });
    });
});

// #endregion
// #endregion

// #region Spirala 4

// #region Predmet
//Create Predmet
app.post('/v2/predmet', function(req, res) {
    db.predmet.create({
        naziv: req.body.naziv
    }).then(function(response) {
        return res.send(JSON.stringify(response));
    }, function(error) {
        return res.send(error.message);
    });
});

//Read Predmet
app.get('/v2/predmet/:id', function(req, res) {
    db.predmet.findByPk(req.params.id).then(function(response) {
        return res.send(JSON.stringify(response));
    }, function(error) {
        return res.send(error.message);
    });
});

//Read Predmeti
app.get('/v2/predmeti', function(req, res) {
    db.predmet.findAll().then(function(response) {
        return res.send(JSON.stringify(response));
    }, function(error) {
        return res.send(error.message);
    });
});

//Update Predmet
app.put('/v2/predmet/:id', function(req, res) {
    db.predmet.findByPk(req.params.id).then(function(response) {
        response.naziv = req.body.naziv;
        response.save().then(function(response) {
            return res.send(JSON.stringify(response));
        }, function(error) {
            return res.send(error.message);
        });
    }, function(error) {
        return res.send(error.message);
    });
});

//Delete Predmet
app.delete('/v2/predmet/:id', function(req, res) {
    db.predmet.destroy({where: {id: req.params.id}}).then(function(response) {
        return res.send(JSON.stringify(response));
    }, function(error) {
        return res.send(error.message);

    });
});
// #endregion

// #region Grupa
//Create Grupa
app.post('/v2/grupa', async function(req, res) {
    db.grupa.create({
        naziv: req.body.naziv,
        PredmetId: req.body.PredmetId
    }).then(function(response) {
        return res.send(JSON.stringify(response));
    }, function(error) {
        return res.send(error.message);
    });
});

//Read Grupa
app.get('/v2/grupa/:id', function(req, res) {
    db.grupa.findByPk(req.params.id).then(function(response) {
        return res.send(JSON.stringify(response));
    }, function(error) {
        return res.send(error.message);
    });
});

//Read Grupe
app.get('/v2/grupe', function(req, res) {
    db.grupa.findAll().then(function(response) {
        return res.send(JSON.stringify(response));
    }, function(error) {
        return res.send(error.message);
    });
});

//Update Grupa
app.put('/v2/grupa', function(req, res) {
    db.grupa.findByPk(req.body.id).then(function(response) {
        response.naziv = req.body.naziv;
        response.PredmetId = req.body.PredmetId;
        response.save().then(function(response) {
            return res.send(JSON.stringify(response));
        }, function(error) {
            return res.send(error.message);
        });
    }, function(error) {
        return res.send(error.message);
    });
});

//Delete Grupa
app.delete('/v2/grupa/:id', function(req, res) {
    db.grupa.destroy({where: {id: req.params.id}}).then(function(response) {
        return res.send(JSON.stringify(response));
    }, function(error) {
        return res.send(error.message);
    });
});
//#endregion

// #region Aktivnost

async function validirajAktivnost(novaAktivnost) {
    let returnValue = false;
    await db.aktivnost.findAll().then(async function(response) {
        let aktivnosti = JSON.parse(JSON.stringify(response));

        const dani = ['Ponedjeljak', 'Utorak', 'Srijeda', 'Četvrtak', 'Petak'];
        if(novaAktivnost.pocetak >= novaAktivnost.kraj 
        || novaAktivnost.pocetak%0.5 != 0 
        || novaAktivnost.kraj%0.5 != 0 
        || novaAktivnost.pocetak == null
        || novaAktivnost.kraj == null
        || novaAktivnost.pocetak < pocetakRasporeda
        || novaAktivnost.kraj > krajRasporeda
        || dani.indexOf(novaAktivnost.dan) == -1 
        || novaAktivnost.naziv == '' 
        || novaAktivnost.tip == ''
        || novaAktivnost.naziv == null
        || novaAktivnost.tip == null) {
            return false;
        }

        for(let aktivnost of aktivnosti) {
            let dan = await db.dan.findOne({where: {id: aktivnost.DanId}});
            if(dan.dataValues.naziv == novaAktivnost.dan) {
                if((aktivnost.pocetak == novaAktivnost.pocetak && aktivnost.kraj == novaAktivnost.kraj) 
                || (aktivnost.pocetak < novaAktivnost.pocetak && aktivnost.kraj > novaAktivnost.pocetak)    // |    p |    k 
                || (aktivnost.pocetak < novaAktivnost.kraj && aktivnost.kraj > novaAktivnost.kraj)          // p    | k    |  
                || (aktivnost.pocetak > novaAktivnost.pocetak && aktivnost.kraj < novaAktivnost.kraj)       // |    p k    |
                || (aktivnost.pocetak < novaAktivnost.pocetak && aktivnost.pocetak > novaAktivnost.kraj)) { // p    | |    k
                    return false;
                }
            }
        }
        returnValue = true;
    });
    return returnValue;
}

//Create Aktivnost
app.post('/v2/aktivnost', async function(req, res) {
    let novaAktivnost = req.body;
    // novaAktivnost.naziv
    // novaAktivnost.tip
    // novaAktivnost.dan
    novaAktivnost.naziv = (await db.predmet.findOne({where: {id: novaAktivnost.PredmetId}})).dataValues.naziv;
    novaAktivnost.tip = (await db.tip.findOne({where: {id: novaAktivnost.TipId}})).dataValues.naziv;
    novaAktivnost.dan = (await db.dan.findOne({where: {id: novaAktivnost.DanId}})).dataValues.naziv;

    let aktivnostJeValidna = await validirajAktivnost(novaAktivnost);
    if(!aktivnostJeValidna) {
        return res.json({message: 'Aktivnost nije validna!'});
    }

    db.aktivnost.create({
        naziv: req.body.naziv,
        pocetak: req.body.pocetak,
        kraj: req.body.kraj,
        PredmetId: req.body.PredmetId,
        GrupaId: req.body.GrupaId,
        DanId: req.body.DanId,
        TipId: req.body.TipId
    }).then(function(response) {
        return res.send(JSON.stringify(response));
    }, function(error) {
        return res.send(error);
    });
});

//Read Aktivnost
app.get('/v2/aktivnost/:id', function(req, res) {
    db.aktivnost.findByPk(req.params.id).then(function(response) {
        return res.send(JSON.stringify(response));
    }, function(error) {
        return res.send(error.message);
    });
});

//Read Aktivnosti
app.get('/v2/aktivnosti', function(req, res) {
    db.aktivnost.findAll().then(function(response) {
        return res.send(JSON.stringify(response));
    }, function(error) {
        return res.send(error.message);
    });
});

//Update Aktivnost
app.put('/v2/aktivnost/:id', function(req, res) {
    if(!validirajAktivnost(req.body)) {
        return res.json({message: 'Aktivnost nije validna!'});
    }

    db.aktivnost.findByPk(req.params.id).then(function(response) {
        response.naziv = req.body.naziv;
        response.pocetak = req.body.pocetak;
        response.kraj = req.body.kraj;
        response.PredmetId = req.body.PredmetId;
        response.GrupaId = req.body.GrupaId;
        response.DanId = req.body.DanId;
        response.TipId = req.body.TipId;
        response.save().then(function(response) {
            return res.send(JSON.stringify(response));
        }, function(error) {
            return res.send(error.message);
        });
    }, function(error) {
        return res.send(error.message);
    });
});

//Delete Aktivnost
app.delete('/v2/aktivnost/:id', function(req, res) {
    db.aktivnost.destroy({where: {id: req.params.id}}).then(function(response) {
        return res.send(JSON.stringify(response));
    }, function(error) {
        return res.send(error.message);
    });
});
//#endregion

// #region Dan
//Create Dan
app.post('/v2/dan', function(req, res) {
    db.dan.create({
        naziv: req.body.naziv
    }).then(function(response) {
        return res.send(JSON.stringify(response));
    }, function(error) {
        return res.send(error.message);
    });
});

//Read Dan
app.get('/v2/dan/:id', function(req, res) {
    db.dan.findByPk(req.params.id).then(function(response) {
        return res.send(JSON.stringify(response));
    }, function(error) {
        return res.send(error.message);
    });
});

//Read Dani
app.get('/v2/dani', function(req, res) {
    db.dani.findAll().then(function(response) {
        return res.send(JSON.stringify(response));
    }, function(error) {
        return res.send(error.message);
    });
});

//Update Dan
app.put('/v2/dan/:id', function(req, res) {
    db.dan.findByPk(req.params.id).then(function(response) {
        response.naziv = req.body.naziv;
        response.save().then(function(response) {
            return res.send(JSON.stringify(response));
        }, function(error) {
            return res.send(error.message);
        });
    }, function(error) {
        return res.send(error.message);
    });
});

//Delete Dan
app.delete('/v2/dan/:id', function(req, res) {
    db.dan.destroy({where: {id: req.params.id}}).then(function(response) {
        return res.send(JSON.stringify(response));
    }, function(error) {
        return res.send(error.message);
    });
});
// #endregion

// #region Tip
//Create Tip
app.post('/v2/tip', function(req, res) {
    db.tip.create({
        naziv: req.body.naziv
    }).then(function(response) {
        return res.send(JSON.stringify(response));
    }, function(error) {
        return res.send(error.message);
    });
});

//Read Tip
app.get('/v2/tip/:id', function(req, res) {
    db.tip.findByPk(req.params.id).then(function(response) {
        return res.send(JSON.stringify(response));
    }, function(error) {
        return res.send(error.message);
    });
});

//Read Tipovi
app.get('/v2/tipovi', function(req, res) {
    db.tip.findAll().then(function(response) {
        return res.send(JSON.stringify(response));
    }, function(error) {
        return res.send(error.message);
    });
});

//Update Tip
app.put('/v2/tip/:id', function(req, res) {
    db.tip.findByPk(req.params.id).then(function(response) {
        response.naziv = req.body.naziv;
        response.save().then(function(response) {
            return res.send(JSON.stringify(response));
        }, function(error) {
            return res.send(error.message);
        });
    }, function(error) {
        return res.send(error.message);
    });
});

//Delete Tip
app.delete('/v2/tip/:id', function(req, res) {
    db.tip.destroy({where: {id: req.params.id}}).then(function(response) {
        return res.send(JSON.stringify(response));
    }, function(error) {
        return res.send(error.message);
    });
});
//#endregion

// #region Student
//Create Student
app.post('/v2/student', function(req, res) {
    db.student.create({
        ime: req.body.ime,
        index: req.body.index
    }).then(function(response) {
        return res.send(JSON.stringify(response));
    }, function(error) {
        return res.send(error.message);
    });
});

//Read Student
app.get('/v2/student/:id', function(req, res) {
    db.student.findByPk(req.params.id).then(function(response) {
        return res.send(JSON.stringify(response));
    }, function(error) {
        return res.send(error.message);
    });
});

//Read Studenti
app.get('/v2/studenti', function(req, res) {
    db.student.findAll().then(function(response) {
        return res.send(JSON.stringify(response));
    }, function(error) {
        return res.send(error.message);
    });
});

//Update Student
app.put('/v2/student/:id', function(req, res) {
    db.student.findByPk(req.params.id).then(function(response) {
        response.naziv = req.body.naziv;
        response.index = req.body.index;
        response.save().then(function(response) {
            return res.send(JSON.stringify(response));
        }, function(error) {
            return res.send(error.message);
        });
    }, function(error) {
        return res.send(error.message);
    });
});

//Delete Student
app.delete('/v2/student/:id', function(req, res) {
    db.student.destroy({where: {id: req.params.id}}).then(function(response) {
        return res.send(JSON.stringify(response));
    }, function(error) {
        return res.send(error.message);
    });
});

//Create Studenti
app.post('/v2/studenti', function(req, res) {
    let nizOdgovora = [];
    let noviStudenti = req.body.studenti;
    let novaGrupa = req.body.grupa;

    db.student.findAll().then(async function(response) {
        let novaGrupaModel = await db.grupa.findOne({where: {naziv: novaGrupa}});
        let stariStudenti = JSON.parse(JSON.stringify(response));
        for(let noviStudent of noviStudenti) {
            let stariStudent = stariStudenti.find(student => student.ime == noviStudent.ime && student.index == noviStudent.index);
            if(stariStudent != null) {
                let student = await db.student.findOne({where: {ime: stariStudent.ime, index: stariStudent.index}});
                let grupeStudenta = await student.getGrupe();
                let indexGrupe = -1;
                for(let i = 0; i < grupeStudenta.length; i++) {
                    if(grupeStudenta[i].dataValues.PredmetId == novaGrupaModel.dataValues.PredmetId) {
                        indexGrupe = i;
                    }
                }
                if(indexGrupe != -1) {
                    grupeStudenta.splice(indexGrupe, 1);
                }
                grupeStudenta.push(novaGrupaModel);
                await student.setGrupe(grupeStudenta);             
            } else {
                let vecPostojeciStudent = stariStudenti.find(student => student.index == noviStudent.index);
                if(vecPostojeciStudent != null) {
                    nizOdgovora.push('Student ' + noviStudent.ime + ' nije kreiran jer postoji student ' + vecPostojeciStudent.ime + ' sa istim indexom ' + noviStudent.index);
                } else {
                    db.student.create({
                        ime: noviStudent.ime,
                        index: noviStudent.index
                    }).then(function(response) {
                        response.setGrupe(novaGrupaModel);
                    }, function(error) {
                        return res.send(error.message);
                    });
                }
            }
        }
        res.status(200);
        return res.send(nizOdgovora);
    }, function(error) {
        return res.send(error.message);
    });
});

// #endregion

// #region Zadatak 3

app.post('/v3/predmet', async function(req, res) {
    let noviPredmetNaziv = req.body.naziv;
    let stariPredmet = await db.predmet.findOne({where: {naziv: noviPredmetNaziv}});
    if(stariPredmet == null) {
        db.predmet.create({
            naziv: noviPredmetNaziv
        }).then(function(response) {
            return res.json({message: 'Uspješno dodan predmet!'});
        }, function(error) {
            return res.send(error.message);
        });
    } else {
        return res.json({message: 'Naziv predmeta postoji!'})
    }
});

app.post('/v3/aktivnost', async function(req, res) {
    let novaAktivnost = req.body;

    if(!(await validirajAktivnost(novaAktivnost))) {
        return res.json({message: 'Aktivnost nije validna!'});
    }

    let nazivPredmeta = req.body.naziv;
    let predmetModel = await db.predmet.findOne({where: {naziv: nazivPredmeta}});
    let predmetId = predmetModel.dataValues.id;

    let grupaModel = await db.grupa.findOne({where: {PredmetId: predmetId}});
    let grupaId = null;
    if(grupaModel != null) {
        grupaId = grupaModel.dataValues.id;
    }

    let danNaziv = req.body.dan;
    let dan = await db.dan.findOne({where: {naziv: danNaziv}});
    let danId = dan.dataValues.id;

    let tipNaziv = req.body.tip;
    let tipModel = await db.tip.findOne({where: {naziv: tipNaziv}});
    let tipId = null;
    if(tipModel == null) {
        tipId = (await db.tip.create({naziv: tipNaziv})).dataValues.id;
    } else {
        tipId = tipModel.dataValues.id;
    }

    db.aktivnost.create({
        naziv: req.body.tip,
        pocetak: req.body.pocetak,
        kraj: req.body.kraj,
        PredmetId: predmetId,
        GrupaId: grupaId,
        DanId: danId,
        TipId: tipId
    }).then(function(response) {
        return res.send({message: 'Uspješno dodana aktivnost!'});
    }, function(error) {
        return res.send(error.message);
    });
});


app.delete('/v3/predmet/:naziv', function(req, res) {
    let nazivPredmeta = req.params.naziv;
    fs.readFile('predmeti.txt', function(err, data) {
        if(err) {
            return res.json({message:'Greška - predmet nije obrisan!'});
        } else {
            let regex1 = new RegExp('\n(' + nazivPredmeta + '.*)', 'g');
            let newData = data.toString().replace(regex1, '');
            let regex2 = new RegExp('(' + nazivPredmeta + '.*)', 'g'); //Ako nazivPredmeta postaje jedini predmet u fajlu, mora se izvrsiti regex bez \n;
            newData = newData.toString().replace(regex2, '');
            fs.writeFile('predmeti.txt', newData, function(err) {
                if(err) {
                    return res.json({message:'Greška - predmet nije obrisan!'});
                } else {
                    obrisiAktivnosti(nazivPredmeta, 'Uspješno obrisan predmet!', res);
                }
            });
        }
    });
});
// #endregion

// #endregion

const port = 3000;
app.listen(port);
console.log("Server pokrenut na portu "+ port);