var Raspored = (function() {
    var iscrtajRaspored =  function(div, dani, satPocetak, satKraj) {
        div.style='text-align: center'
        let table = document.createElement('table');
        table.className = 'table-raspored';

        if(satPocetak >= satKraj || satPocetak < 0 || satPocetak > 24 || satKraj < 0 || satKraj > 24 || !Number.isInteger(satPocetak) || !Number.isInteger(satKraj)) {
            div.innerHTML = 'Greška';
        } else { 
            let redSati = table.insertRow(-1);
            
            let brojKolona = dodajSate(redSati, satPocetak, satKraj);
            dodajDane(table, dani, brojKolona);

            div.appendChild(table);
        }
    }

    function dodajSate(redSati, satPocetak, satKraj) {
        let sati = [0,2,4,6,8,10,12,15,17,19,21,23];
        let indeksPocetak = sati.findIndex(element => element >= satPocetak);
        let indeksKraj = sati.findIndex(element => element >= satKraj);
        if(satKraj < sati[indeksKraj]) indeksKraj--;
        let brojKolona = (satKraj-satPocetak)*2;

        let cell = document.createElement('th')
        cell.colSpan = 3;
        redSati.appendChild(cell);
        
        if((satPocetak < 12 && satPocetak % 2 != 0) || (satPocetak > 12 && satPocetak % 2 == 0) || satPocetak == 13) {
            let cell1 = document.createElement('th')      
            cell1.colSpan = 2;
            if(indeksPocetak<5) cell1.innerHTML = '0';
            cell1.innerHTML += satPocetak + ':00';
            redSati.appendChild(cell1);
        } else if(indeksPocetak == indeksKraj) {
            indeksKraj++;
        } 
        for(; indeksPocetak < indeksKraj; indeksPocetak++) {
            let cell1 = document.createElement('th')
            var cell2 = document.createElement('th')        
            cell1.colSpan = 2;
            cell2.colSpan = 2;
            if(indeksPocetak == 6) cell2.colSpan = 4;

            if(indeksPocetak<5) cell1.innerHTML = '0';
            cell1.innerHTML += sati[indeksPocetak] + ':00';
            
            redSati.appendChild(cell1);
            redSati.appendChild(cell2);
        }
        if(cell2 !== undefined) cell2.colSpan = 1;
        return brojKolona;
    }

    function dodajDane(table, dani, brojKolona) {
        for(let dan of dani) {
            let brKolona = brojKolona;
            let red = table.insertRow();
            
            let cell = red.insertCell();
            cell.colSpan = 3;
            cell.innerHTML = dan;

            while(brKolona > 0) {
                red.insertCell();
                brKolona--;
            }
        }
    }

    var dodajAktivnost = function(raspored, naziv, tip, vrijemePocetak, vrijemeKraj, dan) {
        if(raspored == null) {
            // alert('Greška - raspored nije kreiran');
            return 'Greška - raspored nije kreiran';
        }
        let table = raspored.getElementsByTagName('table')[0];
        if(table == null) {
            // alert('Greška - raspored nije kreiran');
            return 'Greška - raspored nije kreiran';
        }
    
        let redovi = table.rows;
        let dani = [];
        let i = 0;
        let first = true;
        for(let red of redovi) {
            if(!first) {
                dani[i] = red.children[0].innerHTML;
                i++;
            }
            first = false;
        }
        if(vrijemePocetak >= vrijemeKraj || vrijemePocetak%0.5 != 0 || vrijemeKraj%0.5 != 0 || dani.indexOf(dan) == -1) {
            // alert('Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin');
            return 'Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin';
        }
        
        let satPocetak, satKraj;
    
        satPocetak = parseInt(redovi[0].children[1].innerHTML.substring(0,2));
        satKraj = parseInt(redovi[0].children[redovi[0].children.length-2].innerHTML.substring(0,2)) + 2;
        
        if(vrijemePocetak < satPocetak || vrijemeKraj > satKraj) {
            // alert('Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin');
            return 'Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin';
        }
    
        let indeksReda = dani.indexOf(dan) + 1;
        let cellovi = redovi[indeksReda].children;
    
        let n = cellovi.length;
        let tempVrijeme = satPocetak;
        for(let i = 1; i<n; i++) {
            let cell = cellovi[i];          
            if(tempVrijeme == vrijemePocetak) {
                if(cell.innerHTML != '') {
                    // alert('Greška - već postoji termin u rasporedu u zadanom vremenu');
                    return 'Greška - već postoji termin u rasporedu u zadanom vremenu';
                }
                break;
            } 
            else if(tempVrijeme >= vrijemePocetak) {
                // alert('Greška - već postoji termin u rasporedu u zadanom vremenu');
                return 'Greška - već postoji termin u rasporedu u zadanom vremenu';
            }
    
            if(cell.innerHTML != 'Prazan') {
                tempVrijeme += cell.colSpan * 0.5;
            }
    
        }
    
        tempVrijeme = satPocetak;
        for(let i = 1; i<n; i++) {
            let cell = cellovi[i];   
            if(tempVrijeme > vrijemePocetak && tempVrijeme < vrijemeKraj) {
                if(cell.innerHTML != '') {
                    // alert('Greška - već postoji termin u rasporedu u zadanom vremenu');
                    return 'Greška - već postoji termin u rasporedu u zadanom vremenu';
                }
            } else if(tempVrijeme > vrijemeKraj) {
                // alert('Greška - već postoji termin u rasporedu u zadanom vremenu');
                return 'Greška - već postoji termin u rasporedu u zadanom vremenu';
            } else if(tempVrijeme == vrijemeKraj) {
                if(cell.innerHTML != '') {
                    // alert('Greška - već postoji termin u rasporedu u zadanom vremenu');
                    return 'Greška - već postoji termin u rasporedu u zadanom vremenu';
                }
                break;
            }
            if(cell.innerHTML != 'Prazan') {
                tempVrijeme += cell.colSpan * 0.5;
            }
        }
        
        tempVrijeme = satPocetak;
        for(let i = 1; i<n; i++) {
            let cell = cellovi[i];   
            if(tempVrijeme == vrijemePocetak) {
                let cellIndex = Array.prototype.indexOf.call(redovi[indeksReda].children, cell);
                cell.innerHTML=naziv + '<br>' + tip;
                cell.colSpan = (vrijemeKraj - vrijemePocetak)*2;
                cell.style.backgroundColor = "#dee6f0";
                let i = cellIndex + 1;
                for(;i<cellIndex + cell.colSpan; i++) {
                    redovi[indeksReda].deleteCell(cellIndex+1);
                }
                if(cell.colSpan % 2 == 0) {
                    let prazanCell = redovi[indeksReda].insertCell(cellIndex);
                    prazanCell.colSpan = 0;
                    prazanCell.innerHTML = 'Prazan';
                    prazanCell.className = 'cell-prazan';
                }
                break;
            }
            if(cell.innerHTML != 'Prazan') {
                tempVrijeme += cell.colSpan * 0.5;
            }
        }
        return 'Ok';
    }

    return {
        iscrtajRaspored: iscrtajRaspored,
        dodajAktivnost: dodajAktivnost
    }
}())