let grupaSelect = document.getElementById("grupa");
ucitajGrupe();

function ucitajGrupe() {
    $.get("http://localhost:3000/v2/grupe", function(response) {
        let grupe = JSON.parse(response);
        for(let grupa of grupe) {
            let grupaOpcija = document.createElement("option");
            grupaOpcija.text = grupa.naziv;
            grupaSelect.add(grupaOpcija);
        }
    });
}

function formaFunkcija() {
    let textArea = document.getElementById("studentiCSV");
    let studentiCSVString = textArea.value;
    let studentCSVArray = studentiCSVString.split('\n');
    let studentiJSONArray = [];
    for(let studentCSV of studentCSVArray) {
        if(studentCSV != '') {
            let studentPodaci = studentCSV.split(',');
            studentiJSONArray.push({ime: studentPodaci[0], index: studentPodaci[1]});
        }
    }
    $.post("http://localhost:3000/v2/studenti", {studenti: studentiJSONArray, grupa: grupaSelect.value}, function(response) {
        textArea.value = response.join('\n');
    });
}