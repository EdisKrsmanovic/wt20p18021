let listaPredmeta = [];
let listaAktivnosti = [];

let labela = document.getElementById("labelPoruka");
// labela.innerHTML = "Uspješno!";
ucitajPredmeteIAktivnosti();

function ucitajPredmeteIAktivnosti() {
    listaPredmeta = [];
    listaAktivnosti = [];
    $.ajax("http://localhost:3000/v2/predmeti")
        .done(function(data) {
            for (let predmet of data) { 
                listaPredmeta.push(predmet.naziv);
            }
            $.ajax("http://localhost:3000/v2/aktivnosti")
                .done(function(data) {
                    listaAktivnosti = data;
                })
                .fail(function(error) {
                    labela.innerHTML = error.statusText;
                });
        })
        .fail(function(error) {
            labela.innerHTML = error.statusText;
        });
}

function obrisiPredmet(nazivPredmeta) {
    $.ajax({
        method: "DELETE",
        url: "http://localhost:3000/v3/predmet/" + nazivPredmeta
    })
    .done(function(data) {
        ucitajPredmeteIAktivnosti();
        //uspjesno obrisan predmet
    })
    .fail(function(error) {

    });
}

function posaljiNoviPredmetIAktivnost(nazivPredmeta, nazivAktivnosti, vrijemePocetka, vrijemeKraja, danUSedmici) {
    $.ajax({
        method: "POST",
        url: "http://localhost:3000/v3/predmet",
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify({ naziv: nazivPredmeta })
        })
        .done(function(data) {            
            posaljiAktivnost(nazivPredmeta, nazivAktivnosti, vrijemePocetka, vrijemeKraja, danUSedmici, true);
        })
        .fail(function(error) {
            labela.innerHTML = error.statusText;
        });
}

function posaljiAktivnost(nazivPredmeta, nazivAktivnosti, vrijemePocetka, vrijemeKraja, danUSedmici, predmetJeNov) {
    $.ajax({
        method: "POST",
        url: "http://localhost:3000/v3/aktivnost",
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify({ 
            naziv: nazivPredmeta,
            tip: nazivAktivnosti,
            pocetak: vrijemePocetka,
            kraj: vrijemeKraja,
            dan: danUSedmici
        })
        })
        .done(function(data) {        
            if(data.message == "Aktivnost nije validna!" && predmetJeNov) {
                obrisiPredmet(nazivPredmeta);
            } else {
                ucitajPredmeteIAktivnosti();    
            }
            labela.innerHTML = data.message;
        })
        .fail(function(error) {
            labela.innerHTML = error.statusText;
        });
}

function formaSubmit() {
    let nazivPredmetaInput = document.getElementById('nazivPredmeta');
    let nazivAktivnostiInput = document.getElementById('nazivAktivnosti');
    let vrijemePocetkaInput = document.getElementById('vrijemePocetka');
    let vrijemeKrajaInput = document.getElementById('vrijemeKraja');
    let danUSedmiciInput = document.getElementById('danUSedmici');
    
    let nazivPredmeta = nazivPredmetaInput.value;
    let nazivAktivnosti = nazivAktivnostiInput.value;
    let vrijemePocetka = parseInt(vrijemePocetkaInput.value.substring(0,2)) + parseInt(vrijemePocetkaInput.value.substring(3,5))/60;
    let vrijemeKraja = parseInt(vrijemeKrajaInput.value.substring(0,2)) + parseInt(vrijemeKrajaInput.value.substring(3,5))/60;
    let danUSedmici = danUSedmiciInput.value;

    if(!listaPredmeta.includes(nazivPredmeta)) {
        console.log("a");
        posaljiNoviPredmetIAktivnost(nazivPredmeta, nazivAktivnosti, vrijemePocetka, vrijemeKraja, danUSedmici);
    } else {
        console.log("b");
        posaljiAktivnost(nazivPredmeta, nazivAktivnosti, vrijemePocetka, vrijemeKraja, danUSedmici, false);
    }
} 