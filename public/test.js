let assert = chai.assert;
describe('Raspored', function() {
    describe('dodajAktivnost()', function() {
        it('treba iscrtati primjer iz postavke spirale', function() {
            let okvir=document.createElement('div');
            Raspored.iscrtajRaspored(okvir,['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak'],8,21);
            Raspored.dodajAktivnost(okvir,'WT','predavanje',9,12,'Ponedjeljak');
            Raspored.dodajAktivnost(okvir,'WT','vježbe',12,13.5,'Ponedjeljak');
            Raspored.dodajAktivnost(okvir,'RMA','predavanje',14,17,'Ponedjeljak');
            Raspored.dodajAktivnost(okvir,'RMA','vježbe',12.5,14,'Utorak');
            Raspored.dodajAktivnost(okvir,'DM','tutorijal',14,16,'Utorak');
            Raspored.dodajAktivnost(okvir,'DM','predavanje',16,19,'Utorak');
            Raspored.dodajAktivnost(okvir,'OI','predavanje',12,15,'Srijeda');
            let tabele = okvir.getElementsByTagName('table');
            let tabela = tabele[0];
            let redovi = tabela.getElementsByTagName('tr');
            assert.equal(redovi.length, 6, 'Broj redova treba biti 6');
            assert.equal(redovi[2].children[10].innerHTML, 'RMA<br>vježbe', 'RMA vježbe se trebaju nalaziti u desetoj koloni treceg reda');
        });
        it('treba iscrtati raspored koji je ispunjen u svakoj koloni', function() {
            let okvir=document.createElement('div');
            Raspored.iscrtajRaspored(okvir,['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak'],8,21);
            Raspored.dodajAktivnost(okvir,'WT','predavanje',9,12,'Ponedjeljak');
            Raspored.dodajAktivnost(okvir,'WT','vježbe',12,13.5,'Ponedjeljak');
            Raspored.dodajAktivnost(okvir,'RMA','predavanje',14,17,'Ponedjeljak');
            Raspored.dodajAktivnost(okvir,'RMA','vježbe',12.5,14,'Utorak');
            Raspored.dodajAktivnost(okvir,'DM','tutorijal',14,16,'Utorak');
            Raspored.dodajAktivnost(okvir,'DM','predavanje',16,19,'Utorak');
            Raspored.dodajAktivnost(okvir,'OI','predavanje',12,15,'Srijeda');
            let tabele = okvir.getElementsByTagName('table');
            let tabela = tabele[0];
            let redovi = tabela.getElementsByTagName('tr');
            assert.equal(redovi.length, 6, 'Broj redova treba biti 6');
            assert.equal(redovi[2].children[10].innerHTML, 'RMA<br>vježbe', 'RMA vježbe se trebaju nalaziti u desetoj koloni treceg reda');
        });
        it('treba ispuniti raspored u dvije kolone koje su ujedno i jedine dvije kolone', function() {
            let okvir = document.createElement('div');
            Raspored.iscrtajRaspored(okvir,['Ponedjeljak'], 8 ,9);
            Raspored.dodajAktivnost(okvir,'Predmet1','predavanje',8,9,'Ponedjeljak');
            
            let tabele = okvir.getElementsByTagName('table');
            let tabela = tabele[0]
            let redovi = tabela.getElementsByTagName('tr');
            assert.equal(redovi.length, 2, 'Broj redova treba biti 6');
            assert.equal(redovi[1].children[2].innerHTML, 'Predmet1<br>predavanje', 'Prvi predmet u prvom redu i prvoj koloni');
            assert.equal(redovi[1].children.length, 3, 'Broj kolona treba biti 3');
        });
        it('treba izbaciti alert jer raspored nije kreiran', function() {
            let okvir = document.createElement('div');
            let rezultat = Raspored.dodajAktivnost(okvir,'Predmet1','predavanje',8,9,'Ponedjeljak');
            
            assert.equal(rezultat, 'Greška - raspored nije kreiran', 'Raspored nije kreiran');
        });
        it('treba izbaciti alert jer okvir je null', function() {
            let okvir = null;
            let rezultat = Raspored.dodajAktivnost(okvir,'Predmet1','predavanje',8,9,'Ponedjeljak');
            
            assert.equal(rezultat, 'Greška - raspored nije kreiran', 'Okvir je null');
        });
        it('treba izbaciti grešku jer je vrijemePocetak vece od vrijemeKraj', function() {
            let okvir = document.createElement('div');
            Raspored.iscrtajRaspored(okvir,['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak'],8,10);
            let rezultat = Raspored.dodajAktivnost(okvir,'Predmet1','predavanje',8,7,'Ponedjeljak');
            
            assert.equal(rezultat, 'Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin', 'vrijemePocetak ne spije biti vece ili jednako od vrijemeKraj');
         });
         it('treba izbaciti grešku jer je vrijemePocetak ima decimalni dio koji nije .5', function() {
             let okvir = document.createElement('div');
             Raspored.iscrtajRaspored(okvir,['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak'],8,10);
             let rezultat = Raspored.dodajAktivnost(okvir,'Predmet1','predavanje',8.5,12.5,'Ponedjeljak');
             
             assert.equal(rezultat, 'Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin', 'vrijemePocetak i vrijemeKraj mogu imati samo .5 kao decimalu');
          });
          it('treba izbaciti grešku jer ne postoji dan u koji se pokusava dodati aktivnost', function() {
              let okvir = document.createElement('div');
              Raspored.iscrtajRaspored(okvir,['Utorak','Srijeda','Četvrtak','Petak'],8,21);
              let rezultat = Raspored.dodajAktivnost(okvir,'Predmet1','predavanje',8,12,'Ponedjeljak');
              
              assert.equal(rezultat, 'Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin', 'Dan u koji se pokusava dodati aktivnost mora postojati u rasporedu');
           });
           it('treba izbaciti grešku jer vremena aktivnosti su izvan rasporeda', function() {
               let okvir = document.createElement('div');
               Raspored.iscrtajRaspored(okvir,['Ponedjeljak', 'Utorak','Srijeda','Četvrtak','Petak'],8,12);
               let rezultat = Raspored.dodajAktivnost(okvir,'Predmet1','predavanje',8,13,'Ponedjeljak');
               
               assert.equal(rezultat, 'Greška - u rasporedu ne postoji dan ili vrijeme u kojem pokušavate dodati termin', 'Vremena aktivnosti moraju biti unutar vremena rasporeda');
            });
            it('treba izbaciti grešku jer vrijemePocetak se nalazi unutar neke aktivnosti', function() {
                let okvir = document.createElement('div');
                Raspored.iscrtajRaspored(okvir,['Ponedjeljak', 'Utorak','Srijeda','Četvrtak','Petak'],8,21);
                Raspored.dodajAktivnost(okvir,'Predmet1','predavanje',8,13,'Ponedjeljak');
                let rezultat = Raspored.dodajAktivnost(okvir,'Predmet2','predavanje',12,14,'Ponedjeljak');
                
                assert.equal(rezultat, 'Greška - već postoji termin u rasporedu u zadanom vremenu', 'Vremena aktivnosti moraju biti u slobodnom periodu u rasporedu');
             });
             it('treba izbaciti grešku jer vrijemePocetak se nalazi unutar neke aktivnosti', function() {
                 let okvir = document.createElement('div');
                 Raspored.iscrtajRaspored(okvir,['Ponedjeljak', 'Utorak','Srijeda','Četvrtak','Petak'],8,21);
                 Raspored.dodajAktivnost(okvir,'Predmet1','predavanje',8,13,'Ponedjeljak');
                 let rezultat = Raspored.dodajAktivnost(okvir,'Predmet2','predavanje',12,14,'Ponedjeljak');
                 
                 assert.equal(rezultat, 'Greška - već postoji termin u rasporedu u zadanom vremenu', 'Vremena aktivnosti moraju biti u slobodnom periodu u rasporedu');
              });
              it('treba izbaciti grešku jer vrijemeKraj se nalazi unutar neke aktivnosti', function() {
                  let okvir = document.createElement('div');
                  Raspored.iscrtajRaspored(okvir,['Ponedjeljak', 'Utorak','Srijeda','Četvrtak','Petak'],8,21);
                  Raspored.dodajAktivnost(okvir,'Predmet1','predavanje',12,14,'Ponedjeljak');
                  let rezultat = Raspored.dodajAktivnost(okvir,'Predmet2','predavanje',8,13,'Ponedjeljak');
                  
                  assert.equal(rezultat, 'Greška - već postoji termin u rasporedu u zadanom vremenu', 'Vremena aktivnosti moraju biti u slobodnom periodu u rasporedu');
               });
               it('treba izbaciti grešku jer se pokusava dodati aktivnost tacno preko neke druge aktivnosti', function() {
                   let okvir = document.createElement('div');
                   Raspored.iscrtajRaspored(okvir,['Ponedjeljak', 'Utorak','Srijeda','Četvrtak','Petak'],8,21);
                   Raspored.dodajAktivnost(okvir,'Predmet1','predavanje',12,14,'Ponedjeljak');
                   let rezultat = Raspored.dodajAktivnost(okvir,'Predmet2','predavanje',12,14,'Ponedjeljak');
                   
                   assert.equal(rezultat, 'Greška - već postoji termin u rasporedu u zadanom vremenu', 'Vremena aktivnosti moraju biti u slobodnom periodu u rasporedu');
                });
    });
    describe('iscrtajRaspored()', function() {
        it('treba iscrtati raspored od 8 do 10', function() {
            let okvir=document.createElement('div');
            Raspored.iscrtajRaspored(okvir,['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak'],8,10);
            let tabele = okvir.getElementsByTagName('table');
            let tabela = tabele[0];
            let redovi = tabela.getElementsByTagName('tr');
            assert.equal(redovi.length, 6, 'Broj redova treba biti 6');
            assert.equal(redovi[1].children.length, 5, 'Broj kolona treba biti 5');
        });
        it('treba iscrtati raspored koji ima dva dana', function() {
            let okvir=document.createElement('div');
            Raspored.iscrtajRaspored(okvir,['Ponedjeljak','Utorak'],8,10);
            let tabele = okvir.getElementsByTagName('table');
            let tabela = tabele[0];
            let redovi = tabela.getElementsByTagName('tr');
            assert.equal(redovi[1].children[0].innerHTML, 'Ponedjeljak', 'Prvi dan treba biti Ponedjeljak');
            assert.equal(redovi[2].children[0].innerHTML, 'Utorak', 'Drugi dan treba biti Utorak');
            assert.equal(redovi.length, 3, 'Broj redova treba biti 3');
        });
        it('treba iscrtati raspored koji ima sedam dana', function() {
            let okvir=document.createElement('div');
            Raspored.iscrtajRaspored(okvir,['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak','Subota','Nedjelja'],8,10);
            let tabele = okvir.getElementsByTagName('table');
            let tabela = tabele[0];
            let redovi = tabela.getElementsByTagName('tr');
            assert.equal(redovi[1].children[0].innerHTML, 'Ponedjeljak', 'Prvi dan treba biti Ponedjeljak');
            assert.equal(redovi[2].children[0].innerHTML, 'Utorak', 'Drugi dan treba biti Utorak');
            assert.equal(redovi[2].children[0].innerHTML, 'Utorak', 'Srijeda dan treba biti Srijeda');
            assert.equal(redovi[2].children[0].innerHTML, 'Utorak', 'Četvrtak dan treba biti Četvrtak');
            assert.equal(redovi[2].children[0].innerHTML, 'Utorak', 'Petak dan treba biti Petak');
            assert.equal(redovi[2].children[0].innerHTML, 'Utorak', 'Subota dan treba biti Subota');
            assert.equal(redovi[2].children[0].innerHTML, 'Utorak', 'Nedjelja dan treba biti Nedjelja');
            assert.equal(redovi.length, 8, 'Broj redova treba biti 8');
        });
        it('treba ispisati Greška u div zbog pogrešnog parametra sati 1', function() {
            let okvir=document.createElement('div');
            Raspored.iscrtajRaspored(okvir,['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak','Subota','Nedjelja'],8,8);
            assert.equal(okvir.innerHTML, 'Greška', 'U divu treba pisati greška');
        });
        it('treba ispisati Greška u div zbog pogrešnog parametra sati 2', function() {
            let okvir=document.createElement('div');
            Raspored.iscrtajRaspored(okvir,['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak','Subota','Nedjelja'],8.6,20.5);
            assert.equal(okvir.innerHTML, 'Greška', 'U divu treba pisati greška');
        });
        it('treba ispisati Greška u div zbog pogrešnog parametra sati 3', function() {
            let okvir=document.createElement('div');
            Raspored.iscrtajRaspored(okvir,['Ponedjeljak','Utorak','Srijeda','Četvrtak','Petak','Subota','Nedjelja'],24,0);
            assert.equal(okvir.innerHTML, 'Greška', 'U divu treba pisati greška');
        });
        it('treba ispisati Greška u div zbog pogrešnog parametra sati 4', function() {
            let okvir=document.createElement('div');
            Raspored.iscrtajRaspored(okvir,['Ponedjeljak'],'netačan parametar',1);
            assert.equal(okvir.innerHTML, 'Greška', 'U divu treba pisati greška');
        });
        it('treba iscrtati raspored koji nema redova za dane', function() {
            let okvir=document.createElement('div');
            Raspored.iscrtajRaspored(okvir,[],8,21);
            let tabele = okvir.getElementsByTagName('table');
            let tabela = tabele[0];
            let redovi = tabela.getElementsByTagName('tr');
            assert.equal(redovi.length, 1, 'Broj redova treba biti 1');
        });
        it('treba iscrtati raspored koji ima jedan dan i prazne dvije kolone', function() {
            let okvir=document.createElement('div');
            Raspored.iscrtajRaspored(okvir,['Ponedjeljak'],8,9);
            let tabele = okvir.getElementsByTagName('table');
            let tabela = tabele[0];
            let redovi = tabela.getElementsByTagName('tr');
            assert.equal(redovi.length, 2, 'Broj redova treba biti 2');
            assert.equal(redovi[1].children.length, 3, 'Broj kolona treba biti 3');
        });
        it('treba iscrtati raspored koji ima dva dan i prazne četiri kolone', function() {
            let okvir=document.createElement('div');
            Raspored.iscrtajRaspored(okvir,['Ponedjeljak'],8,10);
            let tabele = okvir.getElementsByTagName('table');
            let tabela = tabele[0];
            let redovi = tabela.getElementsByTagName('tr');
            assert.equal(redovi.length, 2, 'Broj redova treba biti 2');
            assert.equal(redovi[1].children.length, 5, 'Broj kolona treba biti 5');
        });
        it('treba iscrtati raspored od 0 do 24', function() {
            let okvir=document.createElement('div');
            Raspored.iscrtajRaspored(okvir,['Ponedjeljak'],0,24);
            let tabele = okvir.getElementsByTagName('table');
            let tabela = tabele[0];
            let redovi = tabela.getElementsByTagName('tr');
            assert.equal(redovi.length, 2, 'Broj redova treba biti 2');
            assert.equal(redovi[1].children.length, 49, 'Broj kolona treba biti 49');
        });
        it('treba iscrtati raspored od 0 do 1', function() {
            let okvir=document.createElement('div');
            Raspored.iscrtajRaspored(okvir,['Ponedjeljak'],0,1);
            let tabele = okvir.getElementsByTagName('table');
            let tabela = tabele[0];
            let redovi = tabela.getElementsByTagName('tr');
            assert.equal(redovi.length, 2, 'Broj redova treba biti 2');
            assert.equal(redovi[1].children.length, 3, 'Broj kolona treba biti 3');
        });
        it('treba ispisati dvije labele za sate u prvom redu', function() {
            let okvir=document.createElement('div');
            Raspored.iscrtajRaspored(okvir,['Ponedjeljak'],0,5);
            let tabele = okvir.getElementsByTagName('table');
            let tabela = tabele[0];
            let redovi = tabela.getElementsByTagName('tr');
            assert.equal(redovi[0].children[1].innerHTML, '00:00', 'Prva labela treba biti 00:00');
            assert.equal(redovi[0].children[3].innerHTML, '02:00', 'Druga labela treba biti 02:00');
            assert.equal(redovi[0].children[4].innerHTML, '', 'Treća labela ne treba prikazati sate');
        });
        it('treba ispisati  pocetnu vrijednost za sate u prvom redu 1', function() {
            let okvir=document.createElement('div');
            Raspored.iscrtajRaspored(okvir,['Ponedjeljak'],0,1);
            let tabele = okvir.getElementsByTagName('table');
            let tabela = tabele[0];
            let redovi = tabela.getElementsByTagName('tr');
            assert.equal(redovi[0].children[1].innerHTML, '00:00', 'Labela treba biti 00:00 jer raspored mora imati pocetnu vrijednost');
        });
        it('treba ispisati pocetnu vrijednost za sate u prvom redu 2', function() {
            let okvir=document.createElement('div');
            Raspored.iscrtajRaspored(okvir,['Ponedjeljak'],0,3);
            let tabele = okvir.getElementsByTagName('table');
            let tabela = tabele[0];
            let redovi = tabela.getElementsByTagName('tr');
            assert.equal(redovi[0].children[1].innerHTML, '00:00', 'Labela treba biti 00:00 jer raspored mora imati pocetnu vrijednost');
            assert.equal(redovi[0].children[2].innerHTML, '', 'Labela treba prazna jer ne treba prikazivati zadnji sat (03:00)');
        });
        it('treba ispisati pocetnu vrijednost za sate iako pocetno vrijeme ne nalazi u dozvoljenim vrijednostima za ispisivanje', function() {
            let okvir=document.createElement('div');
            Raspored.iscrtajRaspored(okvir,['Ponedjeljak'],1,5);
            let tabele = okvir.getElementsByTagName('table');
            let tabela = tabele[0];
            let redovi = tabela.getElementsByTagName('tr');
            assert.equal(redovi[0].children[1].innerHTML, '01:00', 'Labela treba biti 01:00 jer raspored mora imati pocetnu vrijednost');
            assert.equal(redovi[0].children[3].innerHTML, '', 'Labela treba prazna jer ne treba prikazivati zadnji sat (03:00)');
        });
    });
});
