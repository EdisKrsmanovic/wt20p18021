const Sequelize = require("sequelize");
 
module.exports = function (sequelize, DataTypes) {
    const Dan = sequelize.define('Dan', {
        naziv: {
            type: Sequelize.STRING,
            unique: true,
            allowNull: false
        },
   });
   return Dan;
}
