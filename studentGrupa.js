const Sequelize = require("sequelize");
 
module.exports = function (sequelize, DataTypes) {
    const StudentGrupa = sequelize.define('StudentGrupa', {
       id: {           
            type: Sequelize.INTEGER,
            primaryKey: true,
            autoIncrement: true 
       },
       idStudenta: Sequelize.INTEGER,
       idGrupe: Sequelize.INTEGER
   });
   return StudentGrupa;
}
